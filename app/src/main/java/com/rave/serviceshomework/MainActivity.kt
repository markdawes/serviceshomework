package com.rave.serviceshomework

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import kotlinx.android.synthetic.main.activity_main.btnShowNotification
import kotlinx.android.synthetic.main.activity_main.btnStartForeground
import kotlinx.android.synthetic.main.activity_main.btnStartService
import kotlinx.android.synthetic.main.activity_main.btnStopForeground
import kotlinx.android.synthetic.main.activity_main.btnStopService
import kotlinx.android.synthetic.main.activity_main.tvForegroundService
import kotlinx.android.synthetic.main.activity_main.tvServiceInfo

class MainActivity : AppCompatActivity() {

    val CHANNEL_ID = "channelID"
    val CHANNEL_NAME = "channelName"
    val NOTIFICATION_ID = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        createNotificationChannel()

        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle("Service Notification")
            .setContentText("Time has passed")
            .setSmallIcon(R.drawable.ic_star)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .build()

        val notificationManager = NotificationManagerCompat.from(this)

        btnShowNotification.setOnClickListener {
            notificationManager.notify(NOTIFICATION_ID, notification)
        }

        btnStartForeground.setOnClickListener {
            Intent(this, MyForegroundService::class.java).also {
                startService(it)
                tvForegroundService.text = "Foreground Service running"
            }
        }

        btnStopForeground.setOnClickListener {
            Intent(this, MyForegroundService::class.java).also {
                stopService(it)
                tvForegroundService.text = "Foreground Service stopped"
            }
        }

        btnStartService.setOnClickListener{
            Intent(this, MyBackgroundService::class.java).also {
                startService(it)
                tvServiceInfo.text = "Background Service Running"
            }
        }

        btnStopService.setOnClickListener {
            MyBackgroundService.stopService()
            tvServiceInfo.text = "Background Service Stopped"
        }
    }

    fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(CHANNEL_ID, CHANNEL_NAME,
                NotificationManager.IMPORTANCE_DEFAULT).apply {
                lightColor = Color.GREEN
                enableLights(true)
            }
            val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            manager.createNotificationChannel(channel)
        }
    }
}