package com.rave.serviceshomework

import android.app.IntentService
import android.content.Intent
import android.util.Log

class MyBackgroundService : IntentService("MyBackgroundService") {

    init {
        instance = this
    }

    companion object {
        private lateinit var instance: MyBackgroundService
        var isRunning = false
        var n = 0

        fun stopService() {
            Log.d("MyBackgroundService", "Service is stopping...")
            isRunning = false
            instance.stopSelf()
        }
    }

    override fun onHandleIntent(intent: Intent?) {
        try {
            isRunning = true
            while (isRunning) {
                if (n % 3 == 0 && n % 5 == 0) {
                    Log.d("MyBackgroundService", "FizzBuzz")
                } else if (n % 3 == 0) {
                    Log.d("MyBackgroundService", "Fizz")
                } else if (n % 5 == 0) {
                    Log.d("MyBackgroundService", "Buzz")
                } else {
                    Log.d("MyBackgroundService", "$n")
                }
                n++
                Thread.sleep(1000)
            }
        } catch (e: InterruptedException) {
            Thread.currentThread().interrupt()
        }
    }
}