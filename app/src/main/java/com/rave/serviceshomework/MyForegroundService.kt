package com.rave.serviceshomework

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log

class MyForegroundService : Service() {

    val TAG = "MyForegroundService"

    init {
        Log.d(TAG, "Foreground Service is running...")
    }

    override fun onBind(intent: Intent?): IBinder? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        var n = 0
        Thread {
            while (true) {
                if (n % 3 == 0 && n % 5 == 0) {
                    Log.d(TAG, "FizzBuzz")
                } else if (n % 3 == 0) {
                    Log.d(TAG, "Fizz")
                } else if (n % 5 == 0) {
                    Log.d(TAG, "Buzz")
                } else {
                    Log.d(TAG, "$n")
                }
                n++
                Thread.sleep(1000)
            }
        }.start()
        return START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "Foreground Service is being killed...")
    }
}